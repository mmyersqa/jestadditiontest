const addition = require('./addition');

test('adds 1 + 2 to equal 3', () => {
  expect(addition(1, 2)).toBe(3);

});

test('adds negative numbers', () => {
  expect(addition(-1, -1)).toBe(-2);

});

test('adds zero to a number', () => {
  expect(addition(0, 9)).toBe(9);

});
